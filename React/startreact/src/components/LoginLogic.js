import React from "react";
import Title from "./Title";
import PureBtn from "./PureBtn";

const LoginLogic = ({loggedIn, loggedInHandler}) => {
    return (
        <>
        <Title loggedIn={loggedIn} />
        <PureBtn loggedIn={loggedIn} loggedInHandler={loggedInHandler} />
        </>
    )
};

export default LoginLogic