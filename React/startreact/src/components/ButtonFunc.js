import React, { useState } from "react";

// function ButtonFunc() {
//     return (
//         <button className="btn">Кнопка</button>
//     )
// }

// Вариант оформления функционального определения компоненты
// const ButtonFunc2 = () => <button className="btn">Кнопка</button>;

// const ButtonFunc2 = (props) => {
//    return <button className="btn">{props.text}</button>;
// }
// или

const ButtonFunc2 = ({ text= "Кнопка 2", boxHandler, active}) => {
    const [hover, setHover] = useState("btn");
   // const [active, setActive] = useState(true);

   // return <button className="btn" onClick={() => {
   //    setActive(!active);
   // }}
   // >{`${active}`}</button>;
    const hoverStart =() => {
        setHover("btn btn_beautiful");
    };
    const hoverEnd =() => {
        setHover("btn");
    };
   return (
       <button
           className = {hover}
           onClick={() => {
              boxHandler(!active);
           }}
           onMouseEnter={hoverStart}
           onMouseLeave={hoverEnd}
       >{`${active}`}</button>
   );
}

export default ButtonFunc2;