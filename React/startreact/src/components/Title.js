import React from "react";


const Title = ({ loggedIn }) => {

    if (loggedIn) {
        return <h1>Hi, bro</h1>;
    }
    return <h1>Please log in</h1>;
};
export default Title;