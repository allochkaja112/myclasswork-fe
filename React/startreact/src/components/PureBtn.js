import React from "react";

const PureBtn = ({ loggedIn, loggedInHandler }) => {

    const btnClick = () => {
        loggedInHandler(!loggedIn);
    };

    // if (loggedIn) {
    //     return <button onClick={btnClick}>Log out</button>;
    // }
    // return <button onClick={btnClick}>Log in</button>;
    // или
    return <button onClick={btnClick}>{loggedIn ? "Log out" : "Log in"}</button>;
};

export default PureBtn