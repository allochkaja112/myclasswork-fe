import React from "react";

class Button extends React.Component { // Классовая компонента
    // constructor(props) {  //так было раньше до использования
    //     super(props);
    //     this.state = {};
    // }
    state = {
        active: true,
    };

    btnHandler = () => {    //setState - внутренний метод изменения состояния
        this.setState(state => ({
            active: !state.active,
        }));
    }
    render() {
       const { text, beautiful } = this.props;
      const { active } = this.state;
        return (
           <button className={`btn ${beautiful ? "btn_beautiful" : ""}`} onClick={this.btnHandler} >
              { `${active}` }
           </button>
        );
    }
}


export default Button;


// return <button className={`btn ${beautiful ? "btn_beautiful" : ""}`}>{this.props.text || "Кнопка"}</button>;
// render() {
//     const { text, beautiful, link } = this.props;
//     // return <button className={`btn ${beautiful ? "btn_beautiful" : ""}`}>{this.props.text || "Кнопка"}</button>;
//     return (
//         // Обрачиваем, если надо получить не один, а одни общий тег из нескольких параметров
//         <>
//             {  link ? (
//                 <a href={link}>{text}</a>
//             ) : (
//                 <button className={`btn ${beautiful ? "btn_beautiful" : ""}`}>
//                     {text || "Кнопка"}
//                 </button>
//             )}
//         </>
//     );
// }