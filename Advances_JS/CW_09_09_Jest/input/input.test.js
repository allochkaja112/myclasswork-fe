const  inputLoger = require('./input');

describe('Test input logic', function () {
    test("Check input value", () => {
        const testData = {
            target: {
                value: "+380504800000",
            },
        };
        expect(inputLoger(testData)).toBe("+380504800000");
    });
    test("Check input value for text", () => {
        const testData = {
            target: {
                value: "adaddsd",
            },
        };
        expect(inputLoger(testData)).toBe("Not a number");
    });
    test("Check input value for short phone", () => {
        const testData = {
            target: {
                value: "0504800000",
            },
        };
        expect(inputLoger(testData)).toBe("+380504800000");
    });
});