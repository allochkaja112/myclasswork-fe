function filterByTerms(inputArr, searchTerm) {
    // if(searchTerm === "") {
    //     return []
    // }
    const regex = new RegExp(searchTerm, 'i');
    return  inputArr.filter(function (arrayElement) {
        return arrayElement.url.match(regex);
    });
}

module.exports = filterByTerms;