"use strict";

// ЗАДАЧА 1

// let x = 5;
// function first() {
//     let b = 10;
//     function second() {
//         let c = 15;
//         function third() {
//             return c + b + x;
//         }
//         c = 0;
//         return third();
//     }
//     return second();
// }
//
// console.log(first());
//
//
// // ЗАДАЧА 2
// let result = [];
// // for (var i = 0; i < 5; i++) {
// //     result[i] =  function () {
// //         console.log(i);
// //     };
// // }
// // ЧТОБ ИЗБЕЖАТЬПЕРЕЗАПИСЫВАНИЕ ГЛОБАЛЬНОЙ ПЕРЕМЕННОЙ var - ЗАМЫКАНИЕ В ОТДЕЛЬНОЙ ФУНКЦИИ
// for (var i = 0; i < 5; i++) {
//     result[i] =  (function (x) {
//         return function () {
//             console.log(x);
//         };
//     })(i);
// }
// result[0]();
// result[1]();
// result[2]();
// result[3]();
// result[4]();
//
// // ЗАДАЧА 3
//
// function globTest(num, obj) {
//     let array = [1, 2, 3];
//
//     function doSome(i) {
//         num += i;
//
//         array.push(num);
//         console.log(`num: ${num}`);
//         console.log(`array: ${array}`);
//         console.log(`obj.value: ${obj.value}`);
//
//
//     }
//     return  doSome;
// }
//
// let refObj = {value: 10};
//
// let foo = globTest(2, refObj);
// let bar = globTest(6, refObj);
//
// bar(2);
// refObj.value++;
// foo(4);
// bar(4);
//
// (() => {})(); // самовызывающаяся функция, лучше использовать аргументы
// (function(i) {})();


// ЗАДАЧА 4
// function urlGenerator(domain) {
//     return function (url) {
//         return `https://${url}.${domain}`
//     }
// }
// const comUrl = urlGenerator('com');
// const uaUrl = urlGenerator('ua');
//
// console.log(comUrl('google'));
// console.log(comUrl('netflix'));
// console.log(uaUrl('yandex'));

// Необходимо создать собственную функцию bind, которая принимает два аругмента bind(person3, logPerson);

// function logPerson(name, age, job) {
//     this.name = name;
//     this.age = age;
//     this.job = job;
//     console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
// }

// 1 вариант Обычный  метод bind

// const person1 = {};
// logPerson.bind(person1,'Mick', 22,'front')();


// 2 вариант использование замыкания

// function bind(context, fn) {
//     return function (...args) {
//         fn.apply(context, args)
//     }
// }
// function logPerson() {
//     console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
// }
//
// const person2 = {name: 'Liza', age: 25, job: 'SMM'};
// bind(person2, logPerson)();
//
//
//
// // try_catch_finally
//
// try {
//     throw new Error('ohhhhhhh');
// }
//     catch (e) {
//         console.log(e);
// }
//
// let login = document.querySelector('.login');
// let btn = document.querySelector('.btn');
// const  admin = 'Vasia';
//
// function UserException(message) {
//     this.message = message;
//     this.name = "UserException"
// }
// login.addEventListener('click', (e) => {
//     try {
//         if (login.value !== admin) {
//             throw new UserException("Go out");
//         } else if (login.value === '') {
//             throw new UserException("Empty input");
//         } else {
//             console.log("Hello, friend");
//         }
//     } catch (error) {
//         console.log(error);
//     }
// });

//ЗАДАЧА
let test1 = [
    {
        name: 'A',
        role:'user',
        age: 12
    },
    {
        name: 'B',
        age: 20
    },
    {
        name: 'X',
        role:'guest',
        age: 19
    },
    {
        role:'guest',
        age: 30
    },
    {
        name: 'C',
        role: "boss",
        age: 17
    },
];

function checkList(arr) {     // саздание Мар из недостающих ключей субъектов с количеством свойств < 3
    let renderPersons = new Map();
    let samplePerson = ['name', 'role', 'age'];
    arr.forEach((item, i) => {
        let keys = Object.keys(item);
        if (keys.length < 3) {
            let missedProp = samplePerson.filter((prop) => keys.indexOf(prop) === -1);
            renderPersons.set(`${i + 1}`, missedProp);
        }
    });
    return renderPersons;
}
checkList(test1);

// проверка на ошибки
try {
    keyAbsent(checkList(test1));
} catch (error) {
    console.error(error);
}
try {
    underAge(test1);
} catch (e) {
    console.error(e);
}
try {
    checkRole(test1);
} catch (e) {
    console.error(e);
}

function keyAbsent(map) {   // отлавливание недостающего свойства у субъектов, у которых меньше 3 свойств
    let missData = [];
    map.forEach((element, i) => {
       missData.push(`The person ${i} hasn't property "${element}"`);
        });
    throw new Error(`${missData.join(`\n`)}`);
}

function underAge(arr) { // отлавливание недостигших 18 лет по всему массиву
    let underAged = [];
    arr.forEach((item, i) => {
        if (item.age < 18) {
          underAged.push(`The person ${i+1} - no entry, too young`);
        }
    });
    throw new Error(`${underAged.join(`\n`)}`);
}

function checkRole(arr) {  // отлавливание с не надлежащей ролью по всему массиву
    let wrongRole = [];
     arr.forEach((item, i) => {
         if (item.hasOwnProperty('role') && item.role !== 'admin' && item.role !== 'user' && item.role !== 'guest') {
             wrongRole.push(`The person ${i+1} has wrong role`);
         }
        });
        throw new Error(`${wrongRole.join(`\n`)}`)
    }



// let num = +prompt("Введите положительное целое число?", 35);
// let diff, result;
// function fib(n) {
//     if (n < 0 || Math.trunc(n) !== n) {
//         throw new Error("Должно быть целое неотрицательное число");
//     }
//     return n <= 1 ? n : fib(n - 1) + fib(n - 2);
// }
// let start = Date.now();
//
// try {
//     result = fib(num);
// } catch (e) {
//     result = 0;
// } finally {
//     diff = Date.now() - start;
// }
// alert(result || "возникла ошибка");
// alert( `Выполнение заняло ${diff}ms` );

// https://www.youtube.com/watch?v=6_XzKtGPgE8

// let a = 5;
// a = a * 2;
// // document.querySelector('.test').innerHTML = a; // из-за ошибки не выполняется последующий код.
// console.log(a);
// t1();
//
// function t1() {
//    console.log('hello');
// }
//
// try {
//   document.querySelector('.test').innerHTML = a;
// }
// catch (e) {
//     console.warn(e); // выводится тип ошибки, но не красным, а на белом фоне, т.е. работа не прекращается. Если использует WARN - СООБЩЕНИЕ ВЫДЕЛЯАЕТСЯ ЖЕЛТЫМ
//     console.log('1');
// } finally {
//     console.log("Программа отработала все равно")
// }