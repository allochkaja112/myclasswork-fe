// console.log('Start2');
// function timeout2sec() {
//     console.log('timeout2sec')
// }
//
// setTimeout(() => {
//     console.log("Inside timeout after 2 sec")
// }, 3000);
//
// setTimeout(timeout2sec, 2000);
//
// console.log('End') ;

// Вариант без асинк
// function callAPI(url) {
//     const response = fetch(url)
//     .then((data) => {
//     return data.json()
//     }).
//     then((data) => console.log(data))
// }
//
// callAPI("https://jsonplaceholder.typicode.com/users");

// async function callAPI(url) {
//     let response = await fetch(url);
//     console.log( response);
//
// }
//
// callAPI("https://jsonplaceholder.typicode.com/users");


let posts = null;
let users = null;

function searchUserData(id, users) {
    let data = users.filter((el) => {
        if (el.id === id) {
            return {
                name: el.name,
                email: el.email,
            }
        }
    });
    return data
}

async function createCards(posts, users) { // data это объект
    let cardsContainer = document.querySelector('.cards');

    posts.map(({title, body, userId}) => {
        let userData = searchUserData(userId, users);

        let template = `
        <div class="card">
        <span class="card__title">${title}</span>
        <p class = "card__text">${body}</p>
        <span>${userData[0].name}</span>
        <span>${userData[0].email}</span>
        </div>`;

        cardsContainer.insertAdjacentHTML("beforeend", template);
    });
}

async function getUsers() {
    let response = await fetch("https://jsonplaceholder.typicode.com/users");
    let result = response.json();
    return result;
}
async function getPosts() {
    let response = await ("https://jsonplaceholder.typicode.com/posts");
    let result = response.json();
    return result;
}

Promise.all([getUsers(), getPosts()])
    .then((results) => {
        console.log(results);
        users = results[0];
        posts = results[1];
        createCards(posts.data, users.data);
        // console.log(searchUserData(1, users.data));
    })
    .catch((e) => console.log(e));

// let appearCards = async function() {
//     let getUsers = await fetch("https://jsonplaceholder.typicode.com/users");
//     let getPosts = await fetch("https://jsonplaceholder.typicode.com/posts");
//     console.log(getUsers);
//     console.log(getPosts);
//     createCards(getUsers, getPosts);
// };
// appearCards();

