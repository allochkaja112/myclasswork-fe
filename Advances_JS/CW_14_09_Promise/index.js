'use strict';
//
// console.log('Request data');
// setTimeout(() => {
//     console.log('Preparing data...');
//     const  backendData = {
//         server: 'aws',
//         port: 2000,
//         status: 'working'
//     };
//     setTimeout(() => {
//         backendData.modife = true;
//         console.log('Data received', backendData)
//     }, 2000)
// }, 2000);

// то же самое, но с promise

// const  p = new Promise(function (resolve, reject) {
//      setTimeout(() => {
//          console.log('Preparing data...');
//          const  backendData = {
//              server: 'aws',
//              port: 2000,
//              status: 'working'
//          };
//          resolve(backendData) // чтоб передать данные в .then - вставляем в скобочки функции resolve
//      }, 2000);
// });
//
// p.then(data => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             data.modify = true;
//             resolve(data)
//         }, 2000);
//     });
// })
//     .then(clientData => {
//     // console.log('Data received', clientData);
//     clientData.fromPromise = true;
//     return clientData;
// }).then(data => console.log('Modified data'))
// .catch(err => console.error('Error:', err)) // eсли в предыдущем p.then было бы не resolve, а reject, такой кеч поймал бы ошибку и выдал в кансоль
// .finally(() => console.log('Finally')) // Важно!!! В чейнах нельзя ставить ; перед .

let testPro = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Done'), 1000)
});

function wellDone() {
    console.log('Well done');
}
function goWrong() {
    console.log('we lost him');
}
let testPromise = new Promise((wellDone, reject) => {
   setTimeout(() => reject(goWrong()), 1000);
}).catch(err => console.error('Whooops'));


// ПРИМЕР
//Функция для возвращающая промис, которая вызывает метод resolve через то количество мс, которые мы передаем
// const sleep = ms => new Promise(resolve => {
//     setTimeout(() => resolve(), ms)
// });
//
// sleep(2000).then(() => console.log('After 2 sec'));
// sleep(3000).then(() => console.log('After 3 sec'));


// Метод ALL
//
// Promise.all([sleep(2000), sleep(3000)] // Метод all используется когда надо выполнить определенный массив промисов прежде чем  наступит то, что в then
// ).then(() => {
//     console.log('All promises')
// });
//
// // Метод RACE
//
// Promise.race([sleep(2000), sleep(3000)] // Метод race принимает набор промисов и промисы отрабатываются по мере выполнения. Можно определитькакой промис был выполнен первым
// ).then(() => {
//     console.log('Race promises')
// });


// Задача

let delay = ms => new Promise(resolve => {
            setTimeout(() => resolve(`выполнилось через ${ms/1000} секунды`), ms)
        });

delay(3000).then((result) => console.log(result));


//ЗАДАЧА

let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://swapi.dev/api/films');

xhr.send();

xhr.onload = () => {
    console.log(xhr);
    console.log(xhr.response);
    let resp = JSON.parse(xhr.response);
    return resp;
    // resp.forEach(() => {
    //
    // })
    // console.log(resp);
};

xhr.onerror = () => {
    console.log(xhr);
};



//https://www.youtube.com/watch?v=Sr0WT-XHwd0
let a = 7;
// допустим нужно использовать обновленный показатель а

let b = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve (a = 99);
        console.log(a);
    }, 2000);
});
b.then(() => {
    console.log(a);
});
