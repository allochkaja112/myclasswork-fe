'use strict';
class Car {
    constructor(model, color, volume, maxSpeed) {
        this.model = model;
        this.color = color;
        this.volume = volume;
        this.maxSpeed = 110;
    }
    startEngine() {
        console.log('broom broom');
    }
    getData(param) {
        return this[param];
    }
}
class Mini extends Car {
    constructor(model, color, volume, maxSpeed, doors) {
        super(model,color,volume, maxSpeed);
        this.doors = doors;
        this.maxSpeed = maxSpeed;
    }
}
let MyCar = new Car('Ford', 'red', 1.5);
console.log(MyCar);
// let NewMini = new Mini ('Smart', 'blue', 0.8,120, 5);
//
//
// MyCar.startEngine();
// console.log(MyCar.model);
// console.log(MyCar.color);
// console.log(MyCar.volume);
// console.log(MyCar.getData('model'));
// console.log('-----------------');
// NewMini.startEngine();
// console.log(NewMini.model);
// console.log(NewMini.color);
// console.log(NewMini.volume);
// console.log(NewMini.doors);
// console.log(MyCar);
// console.log(NewMini);
//
// let test = {
//     name: 'Test',
//     getData: function(data) {    // уничерсальный геттер
//         console.log(this.data)
//     },
//     get objData() {
//       console.log(`${this.name}/\s/молодец`)
//     },
// };
//
// //ЗАДАЧА
//
// class Worker {
//     // _rate = 10; //доступ к нему устанавливается только через геттер
//     constructor(name, surname, rate, days) {
//         this._name = name;
//         this._surname = surname;
//         this._rate = rate;
//         this._days = days
//     }
//     get rate() {
//         return this._rate
//     }
//     get Salary() {
//         let payment = this._rate * this._days;
//        return (`${this._name} ${this._surname} получил зарплату ${payment}`);
//     }
//     get getName() {
//         return this._name;
//     }
//     get getSurname() {
//         return this._surname;
//     }
//     get getRate() {
//         return this._rate;
//     }
//     get getDays() {
//         return this._days;
//     }
//     set setRate(rate) {
//         this._rate =  this._rate * 10;
//     }
//     set setDays(days) {
//         this._days =  this._days * 20;
//     }
// }
// let NewWorker = new  Worker('John', "Smith", 50, 21);
// let NewWorker2 = new  Worker('Ivan', "Durak", 5, 1);
// NewWorker.setRate = 10;
// NewWorker.setDays = 2;
//
// console.log(NewWorker.getName, NewWorker.getSurname, NewWorker.getRate, NewWorker.getDays, NewWorker.Salary);
// console.log(NewWorker2.getName, NewWorker2.getSurname, NewWorker2.getRate, NewWorker2.getDays, NewWorker2.Salary);


//МАРИО

const HORIZONTAL_CORDS_VALUES = [0, 1, 2];
let pers = null;
let goomba= null;




class Mario {
    constructor(cords) {
        this._cords = cords
    }

    get cords() {
        return this._cords;
    }

    set cords(value) {
        this._cords = value;
        hidePers();
        appearPers(this._cords);
        // let c0 = document.querySelector('.r4c0');
        // let c1 = document.querySelector('.r4c1');
        // let c2 = document.querySelector('.r4c2');
        //
        // value = this._cords;
        // switch (value) {
        //     case 0: c0.appendChild(mario); break;
        //     case 1: c1.appendChild(mario); break;
        //     case 2: c2.appendChild(mario); break;
        // }
    }

    goLeft() {
        if (this._cords !== 0) {
            this._cords = this._cords - 1;
            hidePers();
            appearPers(this._cords);
        }
    }

    goRight() {
        if (this._cords !== 2) {
            this._cords = this._cords + 1;
            hidePers();
            appearPers(this._cords);
        }
    }
}

class Barrier {
    constructor(cordsX, cordsY) {
        this._cordsX = cordsX;
        this._cordsY = cordsY;
    }
    start() {
        document.querySelector(`.r${this._cordsX}c${this._cordsY}`).insertAdjacentHTML('afterbegin', '<img class="barrier" src="goomba.png" alt="Mario">')
    }
}

function startGame() {
    hidePers();
    pers = new Mario();
    goomba = new Barrier(0, 0);
    pers.cords = 1;
}

function hidePers() {
    let userRow = document.querySelector('.user-row');
    Array.from(userRow.children).forEach((element) => {
        element.innerHTML = "";
    })
}

function appearPers(cord) {
    let userRow = Array.from(document.querySelector('.user-row').children);
    if (HORIZONTAL_CORDS_VALUES.includes(cord)) {
        userRow[cord].insertAdjacentHTML('afterbegin', '<img class="mario" src="photo_2020-09-12_14-16-47.png" alt="Mario">');
    }
}

function persMove(vector) {
    if (vector === 'ArrowLeft') {
        pers.goLeft();
    } else if (vector === 'ArrowRight') {
        pers.goRight();
    }
}
    document.addEventListener('keydown', (e) => {
            console.log(e.key);
            persMove(e.key);
        }
    );


    document.addEventListener('DOMContentLoaded', () => {
        startGame();
    });



