let posts = null;
let users = null;

function searchUserData(id, users) {
    let data = users.filter((el) => {
        if (el.id === id) {
            return {
                name: el.name,
                email: el.email,
            }
        }
    });
    return data
}

function createCards(posts, users) { // data это объект
    let cardsContainer = document.querySelector('.cards');

    posts.map(({title, body, userId}) => {
        let userData = searchUserData(userId, users);

        let template  = `<div class="card"><span class="card__title">${title}</span><p class="card__text">${body}</p><span>${userData[0].name}</span><span>${userData[0].email}</span></div>`;

    cardsContainer.insertAdjacentHTML("beforeend", template);
})
}
function getUsers() {
    return axios.get("https://jsonplaceholder.typicode.com/users");
}
function getPosts() {
    return axios.get("https://jsonplaceholder.typicode.com/posts");
        // .then((response) => {
        // createCards(response.data);
        // console.log(response.data)})
        // .catch((e) => console.log(e));
}

Promise.all([getUsers(), getPosts()])
    .then((results) => {
    console.log(results);
    users = results[0];
    posts = results[1];
    createCards(posts.data, users.data);
    // console.log(searchUserData(1, users.data));
})
    .catch((e) => console.log(e));


