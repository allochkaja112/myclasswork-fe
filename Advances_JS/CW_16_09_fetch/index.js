'use strict';

// let xhr = new XMLHttpRequest();
// let data = JSON.stringify({
//     "userId": 1,
//     "id": 1,
//     "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//     "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
// });
//
// // xhr.open("GET", 'https://jsonplaceholder.typicode.com/albums/1/photos');
// // xhr.send();
// // console.log(xhr);
//
// xhr.open("POST", 'https://jsonplaceholder.typicode.com/posts');
// xhr.setRequestHeader("Content-Type", "application/json");
// console.log(xhr.send(data));
// XMLHttpRequest.send()

function sendReQuest(method, url, data = null) {
    let xhr = new XMLHttpRequest();
    if(method === "GET") {
        xhr.open(method, url);
        xhr.send();
    } else if (method === "POST") {
        let dataToSend = JSON.stringify(data);
        xhr.open(method, url);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(dataToSend);
       }
    // let zz = xhr.response.toString();
    return JSON.parse(xhr.response);
    // return xhr;
}

console.log(sendReQuest("GET", "https://jsonplaceholder.typicode.com/posts/1"));
// console.log(JSON.parse(result.response));


let z = {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
};
console.log(sendReQuest("POST", "https://jsonplaceholder.typicode.com/posts", z));


// ЗАДАЧА
let resp = fetch("https://jsonplaceholder.typicode.com/posts/1");
resp
    .then((response) => {
    // return console.log(response.json());
        return response.json();
    })
    .then((data) => console.log(data));


                    // Объект formData
let data = new FormData();

let resp2 = fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "POST",
    headers: {
        "Content-Type": "application/json;charset=utf-8"
    },
    body: "JSON/",
    });

resp2
    .then((response) => {
        // return console.log(response.json());
        return response.body;
    })
    .then((data) => console.log(data));


//ЗАДАЧА

// let xhr = new XMLHttpRequest();
// xhr.open('GET', 'https://swapi.dev/api/films');
//
// xhr.send();
//
// xhr.onload = () => {
//     console.log(xhr);
//     console.log(xhr.response);
//     let resp = JSON.parse(xhr.response);
//     // resp.forEach(() => {
//     //
//     // })
//     // console.log(resp);
// };
// xhr.onerror = () => {
//     console.log(xhr);
// };
