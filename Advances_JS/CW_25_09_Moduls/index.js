
// moduls

// export 
// variables
export let one = 1;

let two = 2;
let three = 3;
export {two, three};

// Class
export class Person {
    constructor(name) {
        this.name = name
    }
}

// function 
export  function sayHello() {
    console.log("Hello!")
}

// или экспортировать вместе, исопльзуя такое выражение. Тогда перед самими объектами и фунцкциями убрать слово export

export {Person, sayHello};

// rename
export { two as second, Person as Man};

// экспорт по умолчанию default export
export  default  class Person {
    constructor(name) {
        this.name = name
    }
}


// -------------------------------IMPORT
import {one, two} from './file.js';

// default export импортируется без {}
import Person from './file.js';


// rename
export { two as second, Person as Man} from './file.js';


// импорт всех значений в виде объекта
export * as numbers from './file.js';
//и тогда, чтоб в коде файла, в который импортируется код, нужно сделать ссылку numbers.one


