const path = require('path');

module.exports = {
    entry: {
        app: "./src/index.js",
    },
    output: {
        filename: "[name].js", // точка выхода будет называться как точка входа
        path: path.resolve(__dirname, "./dist"),
        publicPath: "/dist",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: "/node-modules",
            }
        ]
    },
    devServer: {
        overlay: true,
    }
};