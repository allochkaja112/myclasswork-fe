'use strict';
// let minivan ={
//     type: 'minivan',
//     engine: true,
//     wheels: 4,
//     doors: 6,
//     color: 'black',
//     startEngine: function () {
//         if (this.engine) {
//             console.log(this.type + "brum-brum")
//         }
//     }
// };
//
// let sedan = {
//     type: 'sedan',
//     engine: true,
//     wheels: 4,
//     doors: 4,
//     color: 'blue',
//     startEngine: function () {
//         if (this.engine) {
//             console.log(this.type + "brum-brum")
//         }
//     }
// };

let Car = function (type, engine = true, wheels = 4, doors = 4, color, startEngine) {
    this.type = type;
    this.engine = engine;
    this.wheels = wheels;
    this.doors = doors;
    this.color = color;
};

Car.prototype.startEngine = function() {
        if (this.engine) {
            console.log(this.type + "brum-brum")
        }
};


let minivan = new Car('minovan', true, 4, 5, "black");
console.log(minivan);
minivan.startEngine();


let Worker = function (name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
};

Worker.prototype.getSalary = function () {
    return console.log(this.rate * this.days)
};

let lena = new Worker('Lena', 'Gaga', 100, 30);
console.log(lena);
lena.getSalary();




let String = function (string, count) {
    this.string = string;
    this.count = count
};

String.prototype.reverse = function() {
    let newString = this.string;
    return newString.split("").reverse().join("");
};

String.prototype.ucFirst = function() {
    let uc = this.string[0].toUpperCase();
    let ucFirst = uc + this.string.slice(1);
    return ucFirst;
};

String.prototype.ucWords = function() {
    return this.string.replace(/(^|\s)\S/g, function(str) {return str.toUpperCase()})  // (^|\s) начало строки ^ или | одиничный символ пустого пространства \s на одиночный символ непустого пространства \S (/g - глобальный поиск по всей строке).
};

let string = new String('bababa sdfsdf', 10);


Object.defineProperty(string, 'zzz', {
    value: 'zzzValue'
    // enumerable:false
});


console.log(string);
console.log(string.reverse());
console.log(string.ucFirst());
console.log(string.ucWords());

console.log(string.toString());
console.log(Object.keys(string));
console.log(Object.getOwnPropertyDescriptors(string));

let myRe = /\bis\b/g;

let test1 = function(string) {
    let x = myRe;
    return string.replace(myRe, `<strong>is</strong>`);
};
console.log(test1('My mother is very kind and understanding. We are real friends'));
