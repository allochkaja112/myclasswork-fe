"use strict";
let position = 0;
let positionReflect = 0;
let slidesNumber = 6;
let track = $(".slider-track");
let btnPrev = $(".slider-previous");
let btnNext = $('.slider-next');
let slideReflect = $(".slider-move-reflection");
let movePosition = 920;
let maxNextPosition = -movePosition * slidesNumber;
let slideReflectWidth = 144 / slidesNumber;
let maxNextReflectPosition = slideReflectWidth * slidesNumber;
let reflectBase = document.querySelector(".slider-base"); // если объявить переменную через $() -  addEventListener с ней не хочет работать. как и event.target, если бросать слушатель на родителя кнопок и бегунка и выдергивать с помощью event.target, c синтаксисом jq не фурычит, с js работает нормально, может я где-то ошибку делаю. Поэтому смешала js и jq))


function slide() {

    reflectBase.addEventListener('click', event => {
        let target = reflectBase.getBoundingClientRect();
        let x = event.clientX - target.left;
        let slideToShow = Math.ceil((x - slideReflectWidth) / slideReflectWidth);
        track.css({transform: `translateX(${-movePosition * slideToShow}px)`});
        slideReflect.css({transform: `translateX(${slideReflectWidth * slideToShow}px)`});
        position = -movePosition * (slideToShow);
        positionReflect = slideReflectWidth * slideToShow;
    });

    btnNext.click(() => {
        position -= movePosition;
        positionReflect += slideReflectWidth;
        if (position === maxNextPosition) {
            position = 0;
        }
        track.css({transform: `translateX(${position}px)`});
        if (positionReflect === maxNextReflectPosition) {
            positionReflect = 0;
        }
        slideReflect.css({transform: `translateX(${positionReflect}px)`});
    });

    btnPrev.click(() => {
        if (position === 0) {
            position = maxNextPosition;
            positionReflect = maxNextReflectPosition;
        }
        positionReflect -= slideReflectWidth;
        position += movePosition;
        track.css({ transform: `translateX(${position}px)` });
        slideReflect.css({ transform: `translateX(${positionReflect}px)` });
    });
}

slide();





