// Задачи из  https://learn.javascript.ru/function-basics#tasks

// Задача 1

// let askA = +prompt("Number 1", '');
// let askB = +prompt("Number 2", '');
// function min(a, b) {
//     if (a > b) {
//         console.log(b);
//     } else console.log(a);
// }
// min(askA, askB);


// Задача 2

// let askX = +prompt("Number X", '');
// let askN = +prompt("power n", '');
//
// // проверка на введение натурального и целого числа
//     while (askX < 1 || !Number.isInteger(askX)) {
//         askX = +prompt("Enter Number X ", '');
//     }
//     while (askN < 1 || !Number.isInteger(askN)) {
//    askN = +prompt("Enter power n ", '');
// }
//
// function pow(x, n) {
//     let result = Math.pow(x, n);
//     console.log(result);
// }
//
// pow(askX, askN);


//     Задача 6
// Создайте функцию avg() , которая будет находить среднее значение по всем своим аргументам (аргументы величины числовые).

// function avg() {
//     let result = 0;
//     for(let i = 0; i < arguments.length; i++ ) {
//         result = result + arguments[i];
//     }
//     return result / arguments.length;
// }
//
// avg(1, 2, 3, 4, 5);



// Задание по отладке https://dan-it.gitlab.io/fe-book/programming_essentials/javascript/lesson5_functions/debugger.html
// По задумке автора, он должен выводить на экран все числа из массива numbers, на которые число 18 (dividend) можно поделить без остатка. Но где-то в коде есть ошибка. Попробуйте ее найти, используя debugger.

// var dividend = 18;
// var numbers = [2, 3, 4, 5, 6, 7, 8, 9];
// var idx;
// for (idx = 0; idx < numbers.length; idx++) {
//     var factor;
//     var divisor = numbers[idx];
//
//     if (dividend % divisor === 0) {
//         factor = true;
//         // не было зацикливания в случае factor = false;
//     } else {
//         continue;
//     }
//     if (factor) {
//         console.log(divisor + ' is a factor of ' + dividend + '!');
//     }
// }

