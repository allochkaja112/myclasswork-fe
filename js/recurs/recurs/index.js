// let testObj = {
//   name: "Jhon",
//   lastName: "Falkon",
//   age: 55,
//   pet: {
//     cat: "afas",
//     dog: "asfas",
//     bird: "asfasf",
//     asdgas: "asfasf",
//     dop: {
//       asdf: "asfas",
//     },
//   },
//   someProp: true,
// };

let i = 0;
function calcObj(obj) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      i = i + 1;
      if (typeof obj[key] === "object") {
        calcObj(obj[key]);
      }
    }
  }
}
calcObj(testObj);
// console.log(i);

let testObj = {
  cat: "1",
  dog: "2",
  bird: "3",
  asdgas: "4",
};

function copyObj(obj) {
  let newObj = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      newObj[key] = obj[key];
    }
  }

  return newObj;
}

let copy = copyObj(testObj);
console.log(copy);
