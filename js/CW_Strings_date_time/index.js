"use strict";

// Задача 1
// Напишите функцию ucFirst(str), возвращающую строку str с заглавным первым символом.

// function ucFirst(str) {
//     if (!str) { return str;
//     } else  return str[0].toUpperCase() + str.slice(1);
// }
//
// console.log(ucFirst(`sherman`));

// Задача 2
// Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или 'XXX', а иначе false. Функция должна быть нечувствительна к регистру (checkSpam('buy ViAgRA now') == true
// checkSpam('free xxxxx') == true
// checkSpam("innocent rabbit") == false

function checkSpam(str) {
    let lowerStr = str.toLowerCase();
    return lowerStr.includes(`viagra`) || lowerStr.includes(`xxx`);
    // while ((lowerStr.includes(`viagra`) || lowerStr.includes(`xxx`))) console.log(true);
}

console.log(checkSpam('buy ViAgRA now'));
console.log(checkSpam('free xx xx'));
console.log(checkSpam('innocent rabbit'));

// Задача 3
// Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, если она превосходит maxlength, заменяет конец str на "…", так, чтобы её длина стала равна maxlength.
//     Результатом функции должна быть та же строка, если усечение не требуется, либо, если необходимо, усечённая строка.

function truncate(str, maxlength) {
    if (str.length > maxlength) {
       return str.slice(0, maxlength-1) + `...`
    } return str;
}
console.log(truncate(`Результатом функции должна быть та же строка, если усечение не требуется`, 55));
console.log(truncate(`Результатом функции должна быть та же строка`, 35));

// Задача 3
// Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем – число.
//     Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять числовое значение и возвращать его.

function extractCurrencyValue(str) {
    return str.slice(1);
}

console.log(extractCurrencyValue(`$1500`));



// TIME and DATE

// Бенчмаркинг

function diffSubtract(date1, date2) {
    return date2 - date1;
}
function diffGetTime(date1, date2) {
    return date2.getTime() - date1.getTime();
}
function bench(f) {
    let date1 = new Date(0);
    let date2 = new Date();

    let start = Date.now();
    for (let i = 0; i < 100000; i++) f(date1, date2);
    return Date.now() - start;
}
console.log( 'Время diffSubtract: ' + bench(diffSubtract) + 'мс' );
console.log( 'Время diffGetTime: ' + bench(diffGetTime) + 'мс' );


// ЗАДАЧА 1 Создайте дату
// Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут. Временная зона – местная.

// let date = new Date(2012, 1, 20, 15, 12);
// console.log(date);

// ЗАДАЧА 2 Покажите день недели
// Напишите функцию getWeekDay(date), показывающую день недели в коротком формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», «СБ», «ВС».

// function getWeekDay(date) {
//     let days = [`ВС`, `ПН`, `ВТ`, `СР`, `ЧТ`, `ПТ`, `СБ`];
//     return days[date.getDay()];
// }
// let date = new Date(2020, 1, 13);
// console.log(date);
// console.log(getWeekDay(date));







function getWeekDay(date) {
    let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
     return (days[date.getDay()]);
}
let date = new Date(2020, 2, 23);
console.log(date);
console.log(getWeekDay(date));




// ЗАДАЧА 3 День недели в европейской нумерации
// В Европейских странах неделя начинается с понедельника (день номер 1), затем идёт вторник (номер 2) и так до воскресенья (номер 7). Напишите функцию getLocalDay(date), которая возвращает «европейский» день недели для даты date.

// function getLocalDay(date) {
    // 1 вариант
    // let days = [0, 1, 2, 3, 4, 5, 6];
    // return days[date.getDay()];

    // 2 вариант
//     let day = date.getDay();
//     if (day == 0) {
//         day = 7;
//     }
//     return day;
// }
// let date = new Date(2020, 1, 13);
// console.log(getLocalDay(date));

// ЗАДАЧА 4 Какой день месяца был много дней назад?
// Создайте функцию getDateAgo(date, days), возвращающую число, которое было days дней назад от даты date.
//     К примеру, если сегодня двадцатое число, то getDateAgo(new Date(), 1) вернёт девятнадцатое и getDateAgo(new Date(), 2) – восемнадцатое.
//     Функция должна надёжно работать при значении days=365 и больших значениях:

// function getDateAgo(date, days) {
//     let dateCopy = new Date(date);
//         dateCopy.setDate(date.getDate() - days);
//     return dateCopy.getDate();   //ааааа, бредятинаааа!
// }
// let date = new Date(2020, 1, 13);
// console.log(getDateAgo(date, 4));


// ЗАДАЧА 5 Последнее число месяца?
// Напишите функцию getLastDayOfMonth(year, month), возвращающую последнее число месяца. Иногда это 30, 31 или даже февральские 28/29.
// Параметры:
//     year – год из четырёх цифр, например, 2012.
// month – месяц от 0 до 11.
// К примеру, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль).

// function getLastDayOfMonth(year, month) {
//     let date = new Date(year, month + 1, 0);
//     return date.getDate();
// }
// console.log(getLastDayOfMonth(2020, 1));

// ЗАДАЧА 6 Сколько сегодня прошло секунд?
// Напишите функцию getSecondsToday(), возвращающую количество секунд с начала сегодняшнего дня.
//     Например, если сейчас 10:00, и не было перехода на зимнее/летнее время, то:
// getSecondsToday() == 36000 // (3600 * 10)
// Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.

// function getSecondsToday() {
// //     // 1 вариант
//     let now = new Date();
//     return now.getHours() * 3600 + now.getMinutes() * 60 + now.getSeconds();

//     // 2 вариант
//     // let now = new Date();
//     // let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
//     // let howPass = now - today;
//     // return Math.round(howPass / 1000);
// }

// console.log(getSecondsToday()); // прошло сегодняшнего дня


// ЗАДАЧА 7 Сколько секунд осталось до завтра?
// Создайте функцию getSecondsToTomorrow(), возвращающую количество секунд до завтрашней даты.
//     Например, если сейчас 23:00, то: getSecondsToTomorrow() == 3600
// P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.

// function getSecondsToTomorrow() {
//     let now = new Date();
//     let tomorrow = new Date(now.getFullYear(), now.getMonth(), (now.getDate()+1));
//     let left = tomorrow - now;
//     return Math.round(left / 1000);
// }
// console.log(getSecondsToTomorrow()); // осталось до завтра
//
// console.log(getSecondsToday() + getSecondsToTomorrow());


// ЗАДАЧА 8 Форматирование относительной даты
// Напишите функцию formatDate(date), форматирующую date по следующему принципу:
//     Если спустя date прошло менее 1 секунды, вывести "прямо сейчас".
//     В противном случае, если с date прошло меньше 1 минуты, вывести "n сек. назад".
//     В противном случае, если меньше часа, вывести "m мин. назад".
//     В противном случае, полная дата в формате "DD.MM.YY HH:mm". А именно: "день.месяц.год часы:минуты", всё в виде двух цифр, т.е. 31.12.16 10:00.

// function formatDate(date) {
//     let difference = new Date() - date;
//    if (difference < 1000) {
//        return "прямо сейчас";
//
//    }
//    let sec = Math.floor(difference / 1000);
//    if (sec < 60) {
//        sec = sec * 1000;
//        return `${sec} сек. назад`;
//    }
//    let min = Math.floor(difference / 1000);
//    if (min < 60) {
//        return `${min} мин. назад`;
//    }
//
//     let d = date;
//     d = [
//         '0' + d.getDate(),
//         '0' + (d.getMonth() + 1),
//         '' + d.getFullYear(),
//         '0' + d.getHours(),
//         '0' + d.getMinutes()
//     ].map(component => component.slice(-2)); // взять последние 2 цифры из каждой компоненты
//
//     // соединить компоненты в дату
//     return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
// }
//
// console.log( formatDate(new Date(new Date - 1)) ); // "прямо сейчас"
//
// console.log( formatDate(new Date(new Date - 30 * 1000)) ); // "30 сек. назад"
//
// console.log( formatDate(new Date(new Date - 5 * 60 * 1000)) ); // "5 мин. назад"
//
// // вчерашняя дата вроде 31.12.2016, 20:00
// console.log( formatDate(new Date(new Date - 86400 * 1000)) );
//
// }

// console.log(`here is the \u00A9`.length);