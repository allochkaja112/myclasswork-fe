const gulp = require('gulp');
const sass = require('gulp-sass');

function scss() {
    return gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("css")); //смотрим  все файлы .scss в папке scss, пропускаем через компелятор и что получится складываем в директорию СSS
}
exports.default = gulp.series(scss);